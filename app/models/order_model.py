from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.sql.functions import user
import re

from sqlalchemy.sql.sqltypes import Date

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:Pythonbackend@localhost:3306/penjualan', echo = True)
Session = sessionmaker(bind = engine)
session = Session()

if session:
    print("Connection Success")

class Orders(Base):
    __tablename__ = 'orders'
    orderId = Column(Integer, primary_key= True)
    jumlahOrder = Column(Integer)
    order_date = Column(Date)
    produkId = Column(Integer)
    customerId = Column(Integer)

    def showOrders(self):
        result = session.query(Orders).all()
        return result
    
    def showOrderById(self, **params):
        orderId = params['orderId']
        result_query = session.query(Orders).filter(Orders.orderId == int(orderId))
        return result_query

    def insertOrder(self, **params):    
        session.add(Orders(**params))
        session.commit()
    
    def updateOrderById(self, **params):
        orderId = params['orderId']
        result_query = session.query(Orders).filter(Orders.orderId == int(orderId)).one()
        result_query.jumlahOrder = params['jumlahOrder']
        session.commit()

        result_query = session.query(Orders).filter(Orders.orderId == int(orderId)).one()
        return result_query

    def deleteOrderById(self, **params):
        orderId = params['orderId']
        result_query = session.query(Orders).filter(Orders.orderId == int(orderId)).one()
        session.delete(result_query)
        session.commit()

        result_query = session.query(Orders).all()
        return result_query