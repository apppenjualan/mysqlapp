from app import app
from app.controllers import order_controller
from flask import Blueprint, request,jsonify

order_blueprint = Blueprint("order_controller", __name__)

@app.route('/orders', methods =["GET"])
def showUsers():
    return order_controller.shows()

@app.route('/order', methods = ["GET"])
def show():
    param = request.json
    return order_controller.showOrder(**param)

@app.route('/order/update', methods = ["POST"])
def update():
    param = request.json
    return order_controller.updateOrder(**param)

@app.route('/order/insert', methods = ["POST"])
def insert():
    param = request.json
    return order_controller.insertNewOrder(**param)

@app.route('/order/delete', methods = ["POST"])
def delete():
    param = request.json
    return order_controller.deleteOrder(**param)

