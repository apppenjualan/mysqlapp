from app.models.order_model import Orders
from flask import Flask, jsonify, request
from flask_jwt_extended import *
import json, datetime


mysqldb = Orders()

def shows():
    dbresult = mysqldb.showOrders()
    result = []
    for item in dbresult:
        order = {
            "orderId" : item.orderId,
            "jumlahOrder" : item.jumlahOrder,
            "order_date" : item.order_date,
            "produkId" : item.produkId,
            "customerId" : item.customerId
           
        }
        result.append(order)
    return jsonify(result)

def showOrder(**params):
    dbresult = mysqldb.showOrderById(**params)
    for item in dbresult:
        order = {
            "orderId" : item.orderId,
            "jumlahOrder" : item.jumlahOrder,
            "order_date" : item.order_date,
            "produkId" : item.produkId,
            "customerId" : item.customerId
           
        }
        return jsonify(order)

def updateOrder(**param):
    mysqldb.updateOrderById(**param)
    return jsonify({"message" : "Success"})

def insertNewOrder(**param):
    mysqldb.insertOrder(**param)
    return jsonify({"message" : "success"})

def deleteOrder(**param):
    mysqldb.deleteOrderById(**param)
    return jsonify({"message" : "Success"})
    